use [labor_sql];
go

--1
SELECT DISTINCT([maker]), [type]
from product
where type='laptop'
ORDER BY [maker] ASC
GO

--2
SELECT model, ram, screen, price
from laptop
where price > 1000
ORDER BY ram ASC, price Desc
GO

--3
SELECT *
FROM printer
WHERE color='y'
ORDER BY price DESC
GO
--4
SELECT model, speed, hd, cd, price
FROM pc
WHERE price<600 and (cd='12x' or cd='24x')
ORDER BY speed DESC
GO
--5
SELECT name, class
FROM ships
ORDER BY name ASC
GO

--6
SELECT code, model, speed, ram, hd, cd, price
FROM pc
WHERE speed>=500 and price <800
ORDER BY price DESC
GO

--7
SELECT [code], [model], [color], [type], [price]
FROM [printer]
WHERE [type]!='Matrix' and [price] <300
ORDER BY type ASC

--8 
SELECT model, speed 
FROM pc
where price>=400 and price<=600
order by hd asc

--9
SELECT model, speed, hd, price
FROM laptop
WHERE screen>=12
ORDER BY price DESC

--10
SELECT model, [type], price
FROM printer
WHERE price<300
ORDER BY [type] DESC

--11
SELECT model, ram, price
FROM laptop
WHERE ram=64
ORDER BY screen ASC

--12
SELECT model, ram, price
FROM pc
WHERE ram>64
ORDER BY hd ASC

--13
SELECT model, speed, price
FROM pc
WHERE speed>=500 and speed <= 750
ORDER BY hd DESC

--14
SELECT point, date, out 
FROM Outcome_o
WHERE out>2000
ORDER BY date DESC

--15
SELECT point, date, inc
FROM Income_o
WHERE inc >=5000 and inc<=10000
ORDER BY inc ASC

--16
SELECT code, point, date, inc
FROM income
WHERE point=1
ORDER BY inc ASC

--17
SELECT code, point, [date], [out]
FROM outcome
WHERE point=2
ORDER BY out ASC

--18
SELECT class, [type], country, numGuns, bore, displacement
FROM classes
WHERE country='Japan'
ORDER BY type DESC

--19
SELECT name, launched
FROM ships
WHERE launched BETWEEN 1920 and 1942
ORDER BY launched DESC

--20
SELECT ship, battle, result
FROM outcomes
WHERE battle='Guadalcanal' and (result='damaged' or result='OK')
ORDER BY ship DESC

--21
SELECT ship, battle, result
FROM outcomes
WHERE result='sunk'
ORDER BY ship DESC

--22
SELECT class, displacement
FROM classes
WHERE displacement >= 40000
ORDER BY type ASC

--23
SELECT trip_no, town_from, town_to
FROM trip
WHERE town_from='London' OR town_to='London'
ORDER BY time_out ASC

--24
SELECT trip_no, plane, town_from, town_to
FROM trip
WHERE plane='TU-134'
ORDER BY time_out DESC

--25
SELECT trip_no, plane, town_from, town_to
FROM trip
WHERE plane!='IL-86'
ORDER BY plane ASC

--26
SELECT trip_no, town_from, town_to
FROM trip
WHERE  town_from!='Rostov' and town_to!='Rostov'
ORDER BY plane ASC

--27
SELECT DISTINCT model
FROM pc
WHERE model like '%1%1%'

--28
SELECT code, point, date, out
FROM outcome
WHERE MONTH ( date ) =3

--29
SELECT point, date, out
FROM outcome_o
WHERE day ( date ) =14
   
--30
SELECT name
From ships
where name like 'W%n'

--31
SELECT name
From ships
where name like '%e%e%'

--32
SELECT name, launched
From ships
where name NOT like '%a'

--33
SELECT name
From battles
where name NOT like '%c' AND name like '% %'

--34
SELECT trip_no,id_comp, plane, town_from, town_to, time_out, time_in
FROM trip
WHERE datepart (hh, time_out)>=12 and datepart (hh, time_out)<=17

--35
SELECT trip_no,id_comp, plane, town_from, town_to, time_out, time_in
FROM trip
WHERE datepart (hh, time_in)>=17 and datepart (hh, time_in)<=23

--36 


--37
SELECT [date]
FROM pass_in_trip
WHERE place like '1%'

--38
SELECT [date]
FROM pass_in_trip
WHERE place like '%c'

--39
SELECT name
FROM passenger
WHERE name like '% C%'

--40
SELECT name
FROM passenger
WHERE name NOT like '% J%'

--41
SELECT ('Середня ціна = ' + CAST(AVG(price) AS CHAR(20))) as avg_price
FROM laptop

--42
SELECT  'Код: '+CAST(code AS nvarchar(20)) as code, 'Модель: '+CAST(model AS nvarchar(20)) as model, 
        'Швидкість: '+CAST(speed AS nvarchar(20)) as speed, 'ОП: '+CAST(ram AS nvarchar(20)) as ram, 
        'Жорсткий диск: '+CAST(hd AS nvarchar(20)) as hd, 'СД-ром: '+CAST(cd AS nvarchar(20)) as cd,
        'Ціна: '+CAST(price AS nvarchar(20)) as price
FROM pc

--43
SELECT code, point, CAST(date as [date]) as date, inc
FROM income

--44
SELECT ship, battle,
        CASE result
            WHEN 'sunk' THEN 'потоплений'
            WHEN 'damaged' THEN 'пошкоджений'
            ELSE 'впорядку'
        END as result
FROM outcomes
GO

--45  
SELECT 'Ряд: '+ LEFT(trim(place), 1) as row, 'Місце: '+ right (trim(place), 1) as place
FROM pass_in_trip

--46
SELECT trip_no, id_comp, plane, CONCAT ('From ', RTRIM (town_from), ' to ', RTRIM(town_to)) AS from_to, time_out, time_in
FROM trip

--47                                                       
SELECT LEFT(trim(CONVERT(nvarchar(5), trip_no)), 1) + right(trim(CONVERT(nvarchar(5), trip_no)), 1)+
        LEFT(trim(CONVERT(nvarchar(5), id_comp)), 1) + right(trim(CONVERT(nvarchar(5), id_comp)), 1)+
        LEFT(trim(CONVERT(nvarchar(10), plane)), 1) + right(trim(CONVERT(nvarchar(10), plane)), 1)+
        LEFT(trim(CONVERT(nvarchar(10), town_from)), 1) + right(trim(CONVERT(nvarchar(10), town_from)), 1)+
        LEFT(trim(CONVERT(nvarchar(10), town_to)), 1) + right(trim(CONVERT(nvarchar(10), town_to)), 1)+
        LEFT(trim(CONVERT(nvarchar(10), time_out)), 1) + right(trim(CONVERT(nvarchar(10), time_out)), 1)+
        LEFT(trim(CONVERT(nvarchar(10), time_in)), 1) + right(trim(CONVERT(nvarchar(10), time_in)), 1)
FROM trip

--48
SELECT maker, count(model) as count_model
FROM product 
where type='PC'  
group by maker  
having count(model)>=2

--49 
SELECT  town, count (trip_no) as count_trip
From (
select trip_no, town_from town from trip
UNION ALL
select trip_no, town_to town from trip) as trip_town
GROUP BY town

--50
SELECT distinct type, count(model) as count_model
FROM printer 
group by type  

--51
SELECT distinct cd, count(distinct(model)) as count_model
FROM pc
group by cd


--52


--53

--54

--54